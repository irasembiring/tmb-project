import Head from "next/head";
import Page from "../../hoc/securedPage";
import asyncComponent from "../../util/asyncComponent";

const Crypto = asyncComponent(() => import("../../app/dashboard/Crypto"));

export default Page(() => (
  <>
    <Head>
      <title>Crypto Dashborad</title>
    </Head>
    <div className="app-wrapper">
      <Crypto/>
    </div>
  </>
));